# Barbarian Sky Pirates Adventures

## A 5th Edition Dungeons & Dragons Campaign

Barbarian Sky Pirates Adventures is a campaign for 3-5 characters. It is designed with a full party of Barbarians in mind, but should be perfectly workable with any mix of characters.

### Introduction
This campaign is an ongoing adventure series that revolves around a party of barbarians. They start out _in media res_, captured on a flying ship, when they're given an opportunity to escape and capture the ship while flying over a small island.

### Characters
Currently, this adventure assumes that the charcters start at level 5, with a magic item, and a couple of potions. Perhaps, as it evolves, it will add adventures in the beginning to fill in the blanks leading up to the current entry. 

### Notes
This started out as a larf one-off adventure. I'm not sure what it'll turn into! Let's find out together, shall we?

sd.

