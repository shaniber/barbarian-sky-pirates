# Crab Claw Island

## A 5th Edition Dungeons & Dragons Adventure

Crab Claw Island is an adventure for 3-5 fifth level characters. It is designed with a full party of Barbarians in mind, but should be perfectly workable with any mix of characters.

### Introduction
The adventure begins with the characters restrained on a flying ship, their equipment in a crate with them. They've been kidnapped, and are being taken to face *The Boss*. It turns out that the Flesh Golem that they dispatched was the property of The Boss, and he's not happy they broke it. 

The ship's Captain, *Cap'n Wulfa* (see sky-pirate-captain.md), has been taunting them the entire time about how The Boss is gonna have his way with them. They've been flying over the ocean, to The Boss's secret island lair. They're a short ways away, when one of the PCs spots a small spec of green in the distance, directly ahead. A DC 20 perception or investigation check as they're flying over will see a building in the hillside in a clearing in the northeastern section of the island.

Pick one of the PCs, and let them know that they've managed to slip lose from their bonds. They'll have a short window of time as they pass over the island to escape. If they think of it, they can toss their equipment, and it will land on the island, and if they manage to escape, they'll land in Area 1 - Beach Landing. If they don't manage to toss their equipment, they'll still have a chance to reclaim it, when the airship swings around, and lands to retrieve them. 

If they don't try to escape... well, that leads to a whole MESS of issues where they have to fight ALL of the bandits and Cap'n Wulfa and The Berserker all at once. But that might be fun, too!

### Maps
- island (crab-claw-island.pdf)
- clearing (crab-claw-island-clearing.pdf)
- pirate hideout (crab-claw-island-hideout.pdf)
- entrance to tunnels (crab-claw-island-tunnel-entrance.pdf)
- skeleton tunnel (crab-claw-island-tunnel-skeletons.pdf)
- pirate cave (crab-claw-island-pirate-cave.pdf)

### Locations
- Crab Claw Island
    1. Beach Landing
    2. Clearing
    3. Airship Landing Site
    4. Giant Boars
    5. Burnt Out Watch Points
    6. Pirate Hideout Clearing
    7. Pirate Cave
    8. Bridge Crossing  
- Pirate Hideout
    1. Entranceway
    2. Armoury
    3. Mess Hall
    4. Kitchen
    5. Hallway
    6. Entrance to Tunnels
    7. Barracks
    8. First Mate's Barracks
    9. Captain's Barracks
    10. Captain's Commode
- Entrance to Tunnels
- Skeleton Tunnel
- Pirate Cave

### NPCs
- sky pirate captain
- berzerker

### Notes
