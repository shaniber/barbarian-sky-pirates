# Crab Claw Island

## 1. Landing Beach.
When the PCs bail from the airship, they fall thirty or fourty feet into the water. If they make a DC 15 athletics or acrobatics roll, they manage to land in such a way that they don't take damage, otherwise they take 4d4 damage.

If they fail the saving throw, they are also knocked unconscious. Luckily for them, there's friendly dolphins there to take them to the shore.

Once on the shore, they'll be able to see that the airship is already swinging around, getting ready to find a place to land. They won't have much time before they land.

## 2. Clearing.
Here, the PCs will find their equipment, scattered around the clearing, if they shoved it overboard. Otherwise, it's empty of anything other than normal woods stuff.

There is also a dog wandering around the clearing. It's slightly more intelligent than the average doggo, as the pirates that previously used this island as a hideout had it enhanced. It can say a few words in common. If they befriend it, it'll happily take them to a safe place... the pirate hangout. 

## 3. Airship landing site.
This is where the airship lands. To the east are sheer cliffs, 120' high, climbable with a DC 30 climb check. The party can also rappel down them if they have appropriate climbing gear, with signifcantly greater ease.

They can watch the air-sailors leave and head out to search the island for them, leaving only six sailors behind to guard the ship, two on deck, watching with crossbows, two on the ground guarding the gangplank, and two other at the top of the gangplank.

    Bandit
    Medium humanoid (any race), Any Non-lawful Alignment

    Armor Class 12 (Leather Armor)
    Hit Points 11 (2d8+2)
    Speed 30 ft.

    STR 11 (+0)
    DEX 12 (+1)
    CON 12 (+1)
    INT 10 (+0)
    WIS 10 (+0)
    CHA 10 (+0)

    Senses passive Perception 10
    Languages Any One Language (Usually Common)
    Challenge 1/8 (25 XP)

    Actions

    Scimitar : 1d20 + 3 1d6+1
    Crossbow : 1d20 + 3 1d8+1

    https://roll20.net/compendium/dnd5e/Bandit

## 4. Giant Boars.
OH SHIT FOUR GIANT BOARS because the pirates bred them for food and to protect the island or something. 

    Giant Boar
    Large beast, Unaligned

    Armor Class 12 (Natural Armor)
    Hit Points 42 (5d10+5)
    Speed 40 ft.

    STR 17 (+3)
    DEX 10 (+0)
    CON 16 (+3)
    INT 2 (-4)
    WIS 7 (-2)
    CHA 5 (-3)

    Senses passive Perception 8
    Challenge 2 (450 XP)

    Actions

    Tusk 1d20 + 5 2d6+3
    https://roll20.net/compendium/dnd5e/Giant%20Boar

## 5. Burnt Out Watch Points.
These spots are on the highest section of the island, and were previously used as watchpoints to protect the island. The ground for about a quarter mile diamter is burnt black, and the shelters that were there are destroyed.

## 6. Pirate Hideout Clearing.
As the PCs enter this area, a flock of sea birds fly up. This is the entrance to the pirate hideout. The doggo will lead them straight here. 

## 7. Pirate Cave.
This large cave entrance is about 75' down the the cliffs. It's wide enough to sail a ship into. It's accessible from above, with the same climbing / rappelling DC as the cliffs by the Airship Landing Site. (See pirate-cave.md).

## 8. Bridge Crossing.
This is a bridge across the split in the island. It's about 200' long, and sways in the breeze. It's not very safe. PCs must make a DC14 athletics check to avoid slipping or breaking through. 

If they had to go to the airship, they will encounter a group consisting of The Berzerker (see berzerker.md) and 5 Bandits, guarding the bridge on the far side. The Berserker wil bail if he's near death... he's saving his total rage for the final battle in the Pirate Cave!
