# Pirate Cave

This is where the pirates catch up to them, either from the entranceway or the tunnel behind them. If the PCs met The Berserker at the bridge, he shows up in the second round of combat, having followed them there. He's tougher though!

The Sky Captain will be here, as will either 14 or 9 bandits, depending on how many were at the bridge.