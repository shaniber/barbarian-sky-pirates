# Pirate Hideout

This building has been build into a hillside in a clearing. It was attacked by some unknown force years ago, and it's been abandonded since. 

If the PCs met the Doggo, then it will take it straight in. It will also lead them to the Captain in area ten. 

## 1. Entranceway
This room was originally secured from the outside by two 5x5 doors. One stands ajar, while another lies on the floor. There are a few crates here, and some rusty long swords strewn about the floor.

## 2. Armoury
This was the armoury and defense room for the pirate hideout, with arrow slits in the front. One of these has been blown completely inward. There are broken longbows and some arrows scattered around the room, and a couple rusted shields, and moth eaten sets of leather armour, nothing worth scavenging.

## 3. Mess Hall
The main eating area of the hideout. The 25' table is broken in the center, the shelves on the wall have all been pulled down, and busted chairs, cutlery, and plates are strewn about the room.

## 4. Kitchen
There's a rope tied to the ceiling, where a shank of meat likely hung previously. There's no food left. The stove and oven are open, and cold. The food prep table is stained, and the chef's table is overturned in the corner. A human skeleton lays on the floor, a rusted chef's knife beside it.

If the PCs look closely, they'll find a small sack in one of the barrels in the room. It's been chewed on by rats, and contains about 20GP, the long dead chef's secret stash.

## 5. Hallway
The door to this long hallway has been blasted off, laying blackened and burnt about 15' in. The hallway itself is slightly wider than normal, so that two people could walk shoulder to shoulder. Going further into this area will require light, as the meager sunlight from the entranceway is faded.

## 6. Entrance to Tunnels
This large room has numerous broken crates and barrels, and a large, 15' wide staircase leading down. Unfortunately, it's collapsed, less than ten feet, leaving about a 10' drop to the tunnel below.

## 7. Barracks
These are where the crews of the pirate ships would sleep. A horrible smell emenates from the NW room... opening it leads to the latrine. If the door IS opened, every PC in a 10 radius of the door must make a constitution save (DC 16), or vomit, and have disadvantage until they get some fresh air.

Each barracks room has a couple of bunk beds, shattered chests, a small heating stove with a chimney that leads into the ceiling, and meager personal belongings that have been moth eaten and torn apart by rats. A few papers are amongst them, but nothing of real interest... they're mostly letters that hadn't been sent, or that had been received.

## 8. First Mate's Barracks
Rank has its privileges. This is where the first mate would stay. It's basically the same as the other barracks, just a private room.

## 9. Captain's Barracks
Being the captain has GREATER privileges. In this room is a large bed, mostly torn apart by animals. There's a writing desk, where a journal lays open... the captain's off ship log. There's no hint as to what happened here. 

A closed wardrobe contains some dusty finery and working clothes, and a cutlass in a scabbard.

    Cutlass Supreme
    This cutlass functions as a scimitar of supreme make. It is exceptionally light, 
    and examination shows fine etching in the metal. It is made of adamantium, and it
    functions as a magical weapon, without giving a bonus.

    If this sword is attuned to, however, a hidden intelligence emerges. Not a strong 
    one, or dominant, but one that whispers tactical advice into the user's ear. Upon 
    attunement, this cutlass gives +1 to hit and damage. And, for every year that the 
    weilder uses it, the bonus increases by one, to a maxiumum of +3, as the weilder 
    and the embedded intelligence become more in tune with one another. 
    
    Once attuned, the owner is very resistant to giving this weapon up, even if 
    offered a better weapon. If the attunement is broken, the previous weilder is at 
    disadvantage for 10 days on any attack, as they can't quite seem to get used to 
    their new blade. Reattunement to the Cutlass Supreme reverses this immediatly, 
    and if less than ten days has passed, the bonuses are restored as well.

## 10. Captain's Commode
This room emenates a stench similar to the other room, although not as strong or intense. If anyone enters, they'll find a skeleton, missing its hips, laying on the toilet hole, with a dagger embedded between its ribs, and rags around its ankles. The captain was obviously ambushed while taking a crap.

Examining the room finds a ring on the skeletons finger, a _Ring of Evasion_... which clearly didn't help the captain.