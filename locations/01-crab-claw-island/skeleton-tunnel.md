# Skeleton Tunnel

The tunnel is partially collapsed here. A DC 15 perception check or a DC 12 investigation will reveal the sounds of bones rattling past the cave in. OH SHIT, IT'S THE SKELETONS OF THE PIRATES.

15 skeletons

    Skeleton
    Medium undead, Lawful Evil

    Armor Class 13 (Armor Scraps)
    Hit Points 13 (2d8+4)
    Speed 30 ft.

    STR 10 (+0)
    DEX 14 (+2)
    CON 15 (+2)
    INT 6 (-2)
    WIS 8 (-1)
    CHA 5 (-3)

    Vulnerabilities Bludgeoning
    Damage Immunities Poison
    Condition Immunities Exhaustion, Poisoned
    Senses Darkvision 60 Ft., passive Perception 9
    Languages Understands All Languages It Spoke In Life But Can't Speak
    Challenge 1/4 (50 XP)

    Actions

    Shortsword 1d20 + 4 1d6+2
