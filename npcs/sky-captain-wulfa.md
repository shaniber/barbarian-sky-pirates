# Sky Captain Wulfa

Sky Captain Wulfa has been working for *The Boss* for almost a decade, and has worked himself up to be his most trusted agent. He takes the escape of the PCs personally, and will stop at nothing to recover them.

## Sky Captain Wulfa
*Medium human, Chaotic Evil*
___
- **Armor Class** 15 (Studded Leather)
- **Hit Points** 65 (10d8+20)
- **Speed** 30 ft.

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|15 (+2)|16 (+3)|14 (+2)|14 (+2)|11 (+0)|14 (+2)|

**Saving Throws** Str +4, Dex +5, Wis +2  
**Skills** Athletics +4, Deception +4  
**Senses** Passive Perception 10  
**Languages** Common, Draconic  
**Challenge** 2 (450 XP)  

___

### Combat
***Scimitar*** : 1d20+6 / 1d6+3  
***Dagger*** : 1d20+5 / 1d4+3  

___

### Actions
***Multiattack***  
	The captain makes three melee attacks: two with its Scimitar and one with its Dagger. Or the captain makes two Ranged Attacks with its daggers.  
***Scimitar***  
	Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6+3) slashing damage. Wulfa has been using this weapon for the majority of his adult life, and as such, gains +1 to hit with it.  
***Dagger***  
	Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.  

___

### Traits
***Parry***  
	The captain adds 2 to its AC against one melee Attack that would hit it.  To do so, the captain must see the attacker and be wielding a melee weapon.  

___
Based on: https://roll20.net/compendium/dnd5e/Bandit%20Captain
