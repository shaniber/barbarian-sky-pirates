# The Berserker

The Berserker is the enforcer of the airship. He follows the Sky Captain's orders without question. He's a man of few words, choosing to smash, rather than talk. 

### Berserker
**Medium human, Chaotic Evil**
___
- **Armor Class** 15 (Hide Armor)
- **Hit Points** 67 (9d8+27)
- **Speed** 30 ft.

|STR|DEX|CON|INT|WIS|CHA|
|:---:|:---:|:---:|:---:|:---:|:---:|
|16 (+3)|14 (+2)|17 (+3)|9 (-1)|11 (+0)|9 (-1)|

**Senses** Passive Perception 10
**Languages** Common
**Challenge** 3 (450 XP)

___

### Combat
***Greataxe +1*** : 1d20 + 6 / 1d12+4
___

###Traits
***Reckless***   
    At the start of its turn, the berserker can gain advantage on all melee weapon Attack rolls during that turn, but Attack rolls against it have advantage until the start of its next turn.

***Tough Skin***  
     When wearing hide armour, the Berserker gains +1 to their armour class.

___

Based on: https://roll20.net/compendium/dnd5e/Berserker
